defmodule Fetchers.Lobsters do

  @json_header {"Content-Type", "application/json"}

  def call(url) do
    case HTTPoison.post(api_url(), '{ "story": { "url": "#{url}" } }', [@json_header]) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        listings = Poison.Parser.parse!(body)
        Enum.map(listings["similar_stories"], fn(listing) ->
          Map.merge(
            listing,
            %{
              "site" => "Lobste.rs",
              "location" => location_url(listing),
              "score" => listing_score(listing)
            }
          )
        end)
      {:ok, %HTTPoison.Response{status_code: 404}} ->
        raise "Not found :("
      {:ok, %HTTPoison.Response{status_code: 400}} ->
        []
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect reason
        []
    end
  end

  def location_url(listing) do
    listing["short_id_url"]
  end

  def listing_score(listing) do
    # TODO: more betterer
    listing["score"]
  end

  def api_url do
    "https://lobste.rs/stories/check_url_dupe.json"
  end
end
