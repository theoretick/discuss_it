defmodule Fetchers.LobstersTest do
  use ExUnit.Case, async: false

  import Mock

  test "returns listings matching requested URL" do
    url = "https://blog.jessfraz.com/post/you-might-not-need-k8s"
    body = """
      {
        "url": "#{url}",
        "similar_stories": [
          {
            "short_id_url": "https://example.com/s/fabycv",
            "title": "You might not need Kubernetes",
            "url": "#{url}/",
            "score": 42
          }
        ]
      }
    """
    mock_response = %HTTPoison.Response{status_code: 200, body: body}

    with_mock HTTPoison, [post: fn(_url, _body, _headers) -> {:ok, mock_response} end] do
      assert Fetchers.Lobsters.call(url) == [
        %{
         "url" => "#{url}/",
         "location" => "https://example.com/s/fabycv",
         "score" => 42,
         "short_id_url" => "https://example.com/s/fabycv",
         "site" => "Lobste.rs",
         "title" => "You might not need Kubernetes"
        }
      ]
    end
  end
end
